//
//  covid_data_handlerApp.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 11/12/2021.
//

import SwiftUI

@main
struct covid_data_handlerApp: App {
    
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            MasterView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
