//
//  FeaturedPlaylistsNetworkParameters.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation

struct FeaturedPlaylistsNetworkParameters: Codable {
    var country: String
    
    init(country: Country) {
        self.country = country.rawValue
    }
    
    enum Country: String {
        case us
    }
}
