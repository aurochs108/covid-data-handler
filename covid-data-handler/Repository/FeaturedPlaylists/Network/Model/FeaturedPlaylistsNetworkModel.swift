//
//  FeaturedPlaylists.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation

// MARK: - Welcome
struct FeaturedPlaylistsNetworkModel: Codable {
    let message: String
    let playlists: Playlists
    
    // MARK: - Playlists
    struct Playlists: Codable {
        let href: String
        let items: [ItemNetworkModel]
        let limit: Int
        let next: String?
        let offset: Int
        let previous: String?
        let total: Int
    }
}
