//
//  FeaturedPlaylistRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Foundation
import Resolver

extension Resolver {
    static func registerFeaturedPlaylistsRepository() {
        register { FeaturedPlaylistsNetworkRepository() }
        .implements(FeaturedPlaylistsRepositoryProtocol.self)
    }
}
