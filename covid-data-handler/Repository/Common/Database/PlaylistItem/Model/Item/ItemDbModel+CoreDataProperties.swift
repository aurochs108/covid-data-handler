//
//  ItemDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension ItemDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemDbModel> {
        return NSFetchRequest<ItemDbModel>(entityName: "ItemDbModel")
    }

    @NSManaged public var addedAt: Date?
    @NSManaged public var isLocal: Bool
    @NSManaged public var primaryColor: String?
    @NSManaged public var addedBy: AddedByDbModel?
    @NSManaged public var playlistItemOrigin: NSSet?
    @NSManaged public var track: TrackDbModel?
    @NSManaged public var videoThumbnail: VideoThumbnailDbModel?

}

// MARK: Generated accessors for playlistItemOrigin
extension ItemDbModel {

    @objc(addPlaylistItemOriginObject:)
    @NSManaged public func addToPlaylistItemOrigin(_ value: PlaylistItemDbModel)

    @objc(removePlaylistItemOriginObject:)
    @NSManaged public func removeFromPlaylistItemOrigin(_ value: PlaylistItemDbModel)

    @objc(addPlaylistItemOrigin:)
    @NSManaged public func addToPlaylistItemOrigin(_ values: NSSet)

    @objc(removePlaylistItemOrigin:)
    @NSManaged public func removeFromPlaylistItemOrigin(_ values: NSSet)

}

extension ItemDbModel : Identifiable {

}
