//
//  PlaylistItemDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension PlaylistItemDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlaylistItemDbModel> {
        return NSFetchRequest<PlaylistItemDbModel>(entityName: "PlaylistItemDbModel")
    }

    @NSManaged public var href: String?
    @NSManaged public var limit: Int16
    @NSManaged public var next: String?
    @NSManaged public var offset: Int16
    @NSManaged public var previous: String?
    @NSManaged public var total: Int16
    @NSManaged public var items: NSSet?

}

// MARK: Generated accessors for items
extension PlaylistItemDbModel {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: ItemDbModel)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: ItemDbModel)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}

extension PlaylistItemDbModel : Identifiable {

}
