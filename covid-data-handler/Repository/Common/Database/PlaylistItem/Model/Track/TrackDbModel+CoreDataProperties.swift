//
//  TrackDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension TrackDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TrackDbModel> {
        return NSFetchRequest<TrackDbModel>(entityName: "TrackDbModel")
    }

    @NSManaged public var avaliableMarkets: [String]?
    @NSManaged public var discNumber: Int16
    @NSManaged public var durationMS: Int16
    @NSManaged public var episode: Bool
    @NSManaged public var explicit: Bool
    @NSManaged public var href: String?
    @NSManaged public var id: String?
    @NSManaged public var isLocal: Bool
    @NSManaged public var name: String?
    @NSManaged public var popularity: Int16
    @NSManaged public var previewURL: String?
    @NSManaged public var track: Bool
    @NSManaged public var trackNumber: Int16
    @NSManaged public var type: String?
    @NSManaged public var uri: String?
    @NSManaged public var album: AlbumDbModel?
    @NSManaged public var artists: NSSet?
    @NSManaged public var externalIDS: ExternalIDSDbModel?
    @NSManaged public var externalUrls: ExternalUrlsDbModel?
    @NSManaged public var itemOrigin: ItemDbModel?

}

// MARK: Generated accessors for artists
extension TrackDbModel {

    @objc(addArtistsObject:)
    @NSManaged public func addToArtists(_ value: AddedByDbModel)

    @objc(removeArtistsObject:)
    @NSManaged public func removeFromArtists(_ value: AddedByDbModel)

    @objc(addArtists:)
    @NSManaged public func addToArtists(_ values: NSSet)

    @objc(removeArtists:)
    @NSManaged public func removeFromArtists(_ values: NSSet)

}

extension TrackDbModel : Identifiable {

}
