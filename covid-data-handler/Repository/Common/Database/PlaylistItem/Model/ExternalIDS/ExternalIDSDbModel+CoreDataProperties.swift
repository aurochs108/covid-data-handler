//
//  ExternalIDSDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension ExternalIDSDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExternalIDSDbModel> {
        return NSFetchRequest<ExternalIDSDbModel>(entityName: "ExternalIDSDbModel")
    }

    @NSManaged public var isrc: String?
    @NSManaged public var trackOrigin: TrackDbModel?

}

extension ExternalIDSDbModel : Identifiable {

}
