//
//  VideoThumbnailDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension VideoThumbnailDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VideoThumbnailDbModel> {
        return NSFetchRequest<VideoThumbnailDbModel>(entityName: "VideoThumbnailDbModel")
    }

    @NSManaged public var url: String?
    @NSManaged public var itemOrigin: ItemDbModel?

}

extension VideoThumbnailDbModel : Identifiable {

}
