//
//  AlbumDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension AlbumDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AlbumDbModel> {
        return NSFetchRequest<AlbumDbModel>(entityName: "AlbumDbModel")
    }

    @NSManaged public var albumType: String?
    @NSManaged public var avaliableMarkets: [String]?
    @NSManaged public var href: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var releaseDatePrecision: String?
    @NSManaged public var totalTracks: Int16
    @NSManaged public var type: String?
    @NSManaged public var uri: String?
    @NSManaged public var artists: NSSet?
    @NSManaged public var externalUrls: ExternalUrlsDbModel?
    @NSManaged public var images: NSSet?
    @NSManaged public var trackOrigin: TrackDbModel?

}

// MARK: Generated accessors for artists
extension AlbumDbModel {

    @objc(addArtistsObject:)
    @NSManaged public func addToArtists(_ value: AddedByDbModel)

    @objc(removeArtistsObject:)
    @NSManaged public func removeFromArtists(_ value: AddedByDbModel)

    @objc(addArtists:)
    @NSManaged public func addToArtists(_ values: NSSet)

    @objc(removeArtists:)
    @NSManaged public func removeFromArtists(_ values: NSSet)

}

// MARK: Generated accessors for images
extension AlbumDbModel {

    @objc(addImagesObject:)
    @NSManaged public func addToImages(_ value: ImageDbModel)

    @objc(removeImagesObject:)
    @NSManaged public func removeFromImages(_ value: ImageDbModel)

    @objc(addImages:)
    @NSManaged public func addToImages(_ values: NSSet)

    @objc(removeImages:)
    @NSManaged public func removeFromImages(_ values: NSSet)

}

extension AlbumDbModel : Identifiable {

}
