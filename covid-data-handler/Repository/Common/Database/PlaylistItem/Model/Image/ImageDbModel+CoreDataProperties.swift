//
//  ImageDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension ImageDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageDbModel> {
        return NSFetchRequest<ImageDbModel>(entityName: "ImageDbModel")
    }

    @NSManaged public var height: Int16
    @NSManaged public var url: String?
    @NSManaged public var width: Int16
    @NSManaged public var albumOrigin: AlbumDbModel?

}

extension ImageDbModel : Identifiable {

}
