//
//  AddedByDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension AddedByDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AddedByDbModel> {
        return NSFetchRequest<AddedByDbModel>(entityName: "AddedByDbModel")
    }

    @NSManaged public var href: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var type: String?
    @NSManaged public var uri: String?
    @NSManaged public var albumOrigin: AlbumDbModel?
    @NSManaged public var externalUrls: ExternalUrlsDbModel?
    @NSManaged public var itemOrigin: ItemDbModel?
    @NSManaged public var trackOrigin: TrackDbModel?

}

extension AddedByDbModel : Identifiable {

}
