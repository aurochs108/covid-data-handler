//
//  PlaylistItemDbRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//

import Foundation
import CoreData

class PlaylistItemDbRepository {
    private let context: NSManagedObjectContext
    private let mapper = PlaylistItemNetworkToDbMapper()
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func saveOrUpdate(model: PlaylistTracksNetworkModel,
                      playlistId: String) {
        do {
            let playlistItemDbModel = try getObject(playlistId: playlistId)
            deleteModel(model: playlistItemDbModel)
            saveModel(model: model)
        } catch {
            saveModel(model: model)
        }
    }
    
    func getObject(playlistId: String) throws -> PlaylistItemDbModel {
        let fetchRequest: NSFetchRequest<PlaylistItemDbModel>
        fetchRequest = PlaylistItemDbModel.fetchRequest()
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "href CONTAINS %@", playlistId)
        let objectArray: [PlaylistItemDbModel]
        do {
            objectArray = try context.fetch(fetchRequest)
        } catch {
            throw CoreDataError.noRecord
        }
        guard let object = objectArray.first else { throw CoreDataError.noRecord }
        return object
    }
    
    private func deleteModel(model: PlaylistItemDbModel) {
        self.context.delete(model)
        print("Deleted!")
    }
    
    func saveModel(model: PlaylistTracksNetworkModel) {
        _ = mapper.map(input: model,
                       context: context)
        do {
            try context.save()
            print("saved!")
        } catch {
            print("Error while saving playlistItem to db")
        }
    }
}
