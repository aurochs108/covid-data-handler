//
//  AddedByNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 31/01/2022.
//

import Foundation

struct AddedByNetworkModel: Codable,
                            Hashable {
    let externalUrls: ExternalUrlsNetworkModel
    let href: String
    let id: String
    let type: AddedByTypeEnum
    let uri: String
    let name: String?
}

enum AddedByTypeEnum: String, Codable, Hashable {
    case artist
    case user
}
