//
//  ItemNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation

struct ItemNetworkModel: Codable,
                         Hashable,
                         PlaylistModelProtocol {
    let collaborative: Bool
    let description: String?
    let externalUrls: ExternalUrlsNetworkModel
    let href: String
    let id: String
    let images: [ImageNetworkModel]
    let name: String
    let owner: OwnerNetworkModel
    let primaryColor: String?
    let itemPublic: Bool?
    let snapshotID: String?
    let tracks: TracksNetworkModel
    let type: MusicType
    let uri: String
    
    var mainImage: URL? {
        images.first?.url
    }
    
    var artistsNames: String {
        owner.displayName
    }
    
    static func == (lhs: ItemNetworkModel, rhs: ItemNetworkModel) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
