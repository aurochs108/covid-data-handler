//
//  TracksNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation

struct TracksNetworkModel: Codable {
    let href: String
    let total: Int
}
