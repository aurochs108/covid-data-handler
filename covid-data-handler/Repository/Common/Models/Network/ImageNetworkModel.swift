//
//  ImageModelNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation

struct ImageNetworkModel: Codable,
                          Hashable {
    let height: Int?
    let url: URL
    let width: Int?
}
