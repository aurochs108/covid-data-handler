//
//  AlbumNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 31/01/2022.
//

import Foundation

struct AlbumNetworkModel: Codable,
                          Hashable,
                          PlaylistModelProtocol {
    let albumType: MusicType
    let artists: [AddedByNetworkModel]
    let availableMarkets: [String]
    let externalUrls: ExternalUrlsNetworkModel
    let href: String
    let id: String
    let images: [ImageNetworkModel]
    let name, releaseDate: String
    let releaseDatePrecision: ReleaseDatePrecision
    let totalTracks: Int
    let type: MusicType
    let uri: String
    
    var mainImage: URL? {
        images.first?.url
    }
    
    var artistsNames: String {
        let artistsNamesResult = self.artists
            .compactMap { $0.name }
            .joined(separator: " ")
        
        guard artistsNamesResult != "" else { return "Unknown artist" }
        return artistsNamesResult
    }
    
    var description: String? { nil }
    
    var releaseYear: String? {
        switch self.releaseDatePrecision {
        case .year, .month: return releaseDate
        case .day:
            let yearLength = 4
            guard releaseDate.count >= yearLength else { return nil }
            let index = releaseDate.index(releaseDate.startIndex, offsetBy: yearLength)
            let releaseYear = releaseDate.prefix(upTo: index)
            return String(releaseYear)
        }
    }
}
