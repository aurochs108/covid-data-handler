//
//  AlbumTypeEnum.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 31/01/2022.
//

import Foundation

enum MusicType: String,
                Codable,
                Hashable,
                CaseIterable {
    case album
    case album_single
    case single
    case track
    case artist
    case user
    case compilation
    case playlist
}
