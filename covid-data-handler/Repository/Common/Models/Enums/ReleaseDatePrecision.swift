//
//  ReleaseDatePrecision.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 31/01/2022.
//

import Foundation

enum ReleaseDatePrecision: String,
                           Codable,
                           Hashable {
    case day
    case month
    case year
}
