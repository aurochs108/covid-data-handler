//
//  PlaylistItemDbToNetworkMapper.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//

import Foundation

class PlaylistItemDbToNetworkMapper {
    func map(input: PlaylistItemDbModel) -> PlaylistTracksNetworkModel {
        let itemsDbArray = input.items?.allObjects as! [ItemDbModel]
        let itemsArray = itemsDbArray.map { mapItem(input: $0) }
        return .init(href: input.href!,
                     items: itemsArray,
                     limit: Int(input.limit),
                     next: input.next,
                     offset: Int(input.offset),
                     previous: input.previous,
                     total: Int(input.total))
    }
    
    private func mapImage(input: ImageDbModel) -> ImageNetworkModel {
        return .init(height: Int(input.height),
                     url: URL(string: input.url!)!,
                     width: Int(input.width))
    }
    
    private func mapAlbum(input: AlbumDbModel) -> AlbumNetworkModel {
        let imagesDbArray = input.images?.allObjects as! [ImageDbModel]
        let imagesArray = imagesDbArray.map { mapImage(input: $0) }
        
        let artistsDbArray = input.images?.allObjects as! [AddedByDbModel]
        let artistsArray = artistsDbArray.map { mapAddedBy(input: $0) }
        
        return .init(albumType: MusicType.init(rawValue: input.albumType!)!,
                     artists: artistsArray,
                     availableMarkets: input.avaliableMarkets ?? [],
                     externalUrls: mapExternalUrls(input: input.externalUrls!),
                     href: input.href!,
                     id: input.id!,
                     images: imagesArray,
                     name: input.name!,
                     releaseDate: input.releaseDate!,
                     releaseDatePrecision: ReleaseDatePrecision.init(rawValue: input.releaseDatePrecision!)!,
                     totalTracks: Int(input.totalTracks),
                     type: MusicType.init(rawValue: input.type!)!,
                     uri: input.uri!)
    }
    
    private func mapExternalUrls(input: ExternalUrlsDbModel) -> ExternalUrlsNetworkModel {
        .init(spotify: input.spotify!)
    }
    
    private func mapExternalIDS(input: ExternalIDSDbModel?) -> PlaylistTracksNetworkModel.ExternalIDS? {
        guard let input = input else { return nil }
        return .init(isrc: input.isrc!)
    }
    
    private func mapItem(input: ItemDbModel) -> PlaylistTracksNetworkModel.Item {
        return .init(addedAt: input.addedAt!,
                     addedBy: mapAddedBy(input: input.addedBy!),
                     isLocal: input.isLocal,
                     primaryColor: input.primaryColor,
                     track: mapTrack(input: input.track!),
                     videoThumbnail: mapVideoThumbnail(input: input.videoThumbnail!))
    }
    
    private func mapVideoThumbnail(input: VideoThumbnailDbModel) -> PlaylistTracksNetworkModel.VideoThumbnail {
        return .init(url: URL(string: input.url!))
    }
    
    private func mapTrack(input: TrackDbModel) -> PlaylistTracksNetworkModel.Track {
        let addedByDbArray = input.artists?.allObjects as! [AddedByDbModel]
        let addedByArray = addedByDbArray.map { mapAddedBy(input: $0) }
        
        return .init(album: mapAlbum(input: input.album!),
                     artists: addedByArray,
                     availableMarkets: input.avaliableMarkets ?? [],
                     discNumber: Int(input.discNumber),
                     durationMS: Int(input.durationMS),
                     episode: input.episode,
                     explicit: input.explicit,
                     externalIDS: mapExternalIDS(input: input.externalIDS),
                     externalUrls: .init(spotify: input.externalUrls!.spotify!),
                     href: input.href!,
                     id: input.id!,
                     isLocal: input.isLocal,
                     name: input.name!,
                     popularity: Int(input.popularity),
                     previewURL: input.previewURL,
                     track: input.track,
                     trackNumber: Int(input.trackNumber),
                     type: PlaylistTracksNetworkModel.TrackType(rawValue: input.type!)!,
                     uri: input.uri!)
    }
    
    private func mapAddedBy(input: AddedByDbModel) -> AddedByNetworkModel {
        return .init(externalUrls: mapExternalUrls(input: input.externalUrls!),
                     href: input.href!,
                     id: input.id!,
                     type: AddedByTypeEnum(rawValue: input.type!)!,
                     uri: input.uri!,
                     name: input.name)
    }
}
