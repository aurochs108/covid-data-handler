//
//  PlaylistItemsRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//

import Foundation
import Combine
import Alamofire

class PlaylistTracksNetworkRepository: NetworkRepository,
                                       PlaylistTracksRepositoryProtocol {
    func getPlaylistItems(playlistId: String) -> AnyPublisher<ResponseModel<PlaylistTracksNetworkModel>, Error> {
        let url = String(format: ProjectData.Playlists.GET_PLAYLIST_TRACKS, playlistId)
        return super.getModel(url: url)
    }
}
