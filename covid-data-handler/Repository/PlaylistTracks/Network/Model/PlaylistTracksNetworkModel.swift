//
//  PlaylistItemModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/01/2022.
//

import Foundation

struct PlaylistTracksNetworkModel: Codable {
    let href: String
    let items: [Item]
    let limit: Int
    let next: String?
    let offset: Int
    let previous: String?
    let total: Int
    
    // MARK: - Item
    struct Item: Codable, Hashable {
        let addedAt: Date
        let addedBy: AddedByNetworkModel
        let isLocal: Bool
        let primaryColor: String?
        let track: Track
        let videoThumbnail: VideoThumbnail
    }
    
    // MARK: - Track
    struct Track: Codable,
                  Hashable,
                  TrackItem {
        let album: AlbumNetworkModel
        let artists: [AddedByNetworkModel]
        let availableMarkets: [String]
        let discNumber: Int
        let durationMS: Int?
        let episode, explicit: Bool
        let externalIDS: ExternalIDS?
        let externalUrls: ExternalUrlsNetworkModel
        let href: String
        let id: String
        let isLocal: Bool
        let name: String
        let popularity: Int
        let previewURL: String?
        let track: Bool
        let trackNumber: Int
        let type: TrackType
        let uri: String
        
        var artistsNames: String {
            var artistsString: String = ""
            self.artists.compactMap { $0.name }
            .forEach { artistsString += $0 }
            
            guard artistsString != "" else { return "Unknown artist" }
            return artistsString
        }
        
        var mainImage: URL? {
            album.mainImage
        }
    }
    
    // MARK: - ExternalIDS
    struct ExternalIDS: Codable, Hashable {
        let isrc: String
    }
    
    enum TrackType: String, Codable, Hashable {
        case track
    }
    
    // MARK: - VideoThumbnail
    struct VideoThumbnail: Codable, Hashable {
        let url: URL?
    }
}
