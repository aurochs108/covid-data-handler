//
//  PlaylistTracksRepositoryProtocol.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Foundation
import Combine
import Alamofire

protocol PlaylistTracksRepositoryProtocol {
    func getPlaylistItems(playlistId: String) -> AnyPublisher<ResponseModel<PlaylistTracksNetworkModel>, Error>
}
