//
//  AlbumTracksRepositoryProtocol.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Foundation
import Combine
import Alamofire

protocol AlbumTracksRepositoryProtocol {
    func getAlbumTracks(albumId: String) -> AnyPublisher<ResponseModel<AlbumTrackNetworkModel>, Error>
}
