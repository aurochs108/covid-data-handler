//
//  AlbumTracksMockRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 12/03/2022.
//

import Resolver

extension Resolver {
    static func registerAlbumTracksMockRepository() {
        register { AlbumTracksMockRepository() }
        .implements(AlbumTracksRepositoryProtocol.self)
    }
}
