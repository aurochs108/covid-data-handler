//
//  AlbumTrackNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 01/02/2022.
//

import Foundation

struct AlbumTrackNetworkModel: Codable {
    let href: String
    let items: [Item]
    let limit: Int
    let next: String?
    let offset: Int
    let previous: String?
    let total: Int
    
    // MARK: - Item
    struct Item: Codable,
                 Hashable,
                 TrackItem {
        let artists: [AddedByNetworkModel]
        let availableMarkets: [String]
        let discNumber: Int
        let durationMS: Int?
        let explicit: Bool
        let externalUrls: ExternalUrlsNetworkModel
        let href: String
        let id: String
        let isLocal: Bool
        let name: String
        let previewURL: String?
        let trackNumber: Int
        let type: String
        let uri: String
        
        var artistsNames: String {
            var artistsString: String = ""
            self.artists.compactMap { $0.name }
            .forEach { artistsString += $0 }
            
            guard artistsString != "" else { return "Unknown artist" }
            return artistsString
        }
    }
}
