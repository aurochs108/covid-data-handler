//
//  AlbumTracksNetworkRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/03/2022.
//

import Resolver

extension Resolver {
    static func registerAlbumTracksNetworkRepository() {
        register { AlbumTracksNetworkRepository() }
        .implements(AlbumTracksRepositoryProtocol.self)
    }
}
