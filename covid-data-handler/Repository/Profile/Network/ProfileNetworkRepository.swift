//
//  ProfileNetworkRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 29/12/2021.
//

import Foundation
import Combine
import Alamofire

class ProfileNetworkRepository: NetworkRepository,
                                ProfileRepositoryProtocol {
    func getCurrentUserProfile() -> AnyPublisher<ResponseModel<ProfileNetworkModel>, Error> {
        super.getModel(url: ProjectData.Profile.GET_CURRENT_USER_PROFILE)
    }
}
