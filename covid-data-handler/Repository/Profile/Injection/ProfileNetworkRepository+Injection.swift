//
//  ProfileNetworkRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/03/2022.
//

import Foundation
import Resolver

extension Resolver {
    static func registerProfileNetworkRepository() {
        register { ProfileNetworkRepository() }
        .implements(ProfileRepositoryProtocol.self)
    }
}
