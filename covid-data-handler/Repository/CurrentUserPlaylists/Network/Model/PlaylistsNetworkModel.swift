//
//  PlaylistsNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation

struct PlaylistsNetworkModel: Codable {
    let href: String
    let items: [ItemNetworkModel]
    let limit: Int
    let next: String?
    let offset: Int
    let previous: String?
    let total: Int
}
