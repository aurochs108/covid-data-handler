//
//  NewReleasesNetworkRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation
import Combine
import Alamofire

class NewReleasesNetworkRepository: NetworkRepository,
                                    NewReleasesRepositoryProtocol {
    func getNewReleases() -> AnyPublisher<ResponseModel<NewReleaseNetworkModel>, Error> {
        super.getModel(url: ProjectData.Albums.GET_NEW_RELEASES)
    }
}
