//
//  NewReleases.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation

struct NewReleaseNetworkModel: Codable {
    let albums: Albums

    // MARK: - Albums
    struct Albums: Codable {
        let href: String
        let items: [AlbumNetworkModel]
        let limit: Int
        let next: String
        let offset: Int     
        let previous: String?
        let total: Int
    }
}
