//
//  ProjectLinks.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 20/12/2021.
//

import Foundation

class ProjectLinks {
    enum Login {
        static let LOGIN_URL = "https://accounts.spotify.com/authorize"
        static let CALLBACK_URI = "https://en.wikipedia.org/callback"
    }
}
