//
//  ProjectData.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 13/12/2021.
//

import Alamofire

enum ProjectData {
    static let BASE_API_URL_V1 = "https://api.spotify.com/v1/"
}

extension ProjectData {
    enum Auth {
        static let TOKEN = "https://accounts.spotify.com/api/token"
    }
    
    enum Profile {
        static let GET_CURRENT_USER_PROFILE = BASE_API_URL_V1 + "me"
    }
    
    enum Playlists {
        static let GET_CURRENT_USER_PLAYLISTS = BASE_API_URL_V1 + "me/playlists"
        static let GET_FEATURED_PLAYLISTS = BASE_API_URL_V1 + "browse/featured-playlists"
        static let GET_PLAYLIST_TRACKS = BASE_API_URL_V1 + "playlists/%@/tracks"
    }
    
    enum Albums {
        static let GET_NEW_RELEASES = BASE_API_URL_V1 + "browse/new-releases"
        static let GET_ALBUM_TRACKS = BASE_API_URL_V1 + "albums/%@/tracks"
    }
}

