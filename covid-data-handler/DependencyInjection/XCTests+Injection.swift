//
//  Resolver+XCTests.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 10/03/2022.
//

import Resolver

extension Resolver {
    static var mock = Resolver(child: .main)
    
    static func registerMockServices() {
        root = Resolver.mock
        defaultScope = .application
        
        registerAlbumTracksMockRepository()
    }
}

