//
//  HomeViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation
import Combine
import Resolver

class HomeViewModel: BaseViewModel {
    @Injected private var newReleasesPlaylistsNetworkRepository: NewReleasesRepositoryProtocol
    @Injected private var featuredPlaylistsNetworkRepository: FeaturedPlaylistsRepositoryProtocol
    
    var newReleasesPlaylistsModel: NewReleaseNetworkModel!
    var featuredPlaylistsModel: FeaturedPlaylistsNetworkModel!
    
    override init() {
        super.init()
        bindOutput()
    }
    
    private func bindOutput() {
        let getNewReleases = newReleasesPlaylistsNetworkRepository.getNewReleases()
        let getFeaturedPlaylists = featuredPlaylistsNetworkRepository.getFeaturedPlaylists(parameters: .init(country: .us))
        
        getNewReleases
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] newReleasesModel in
                guard let strongSelf = self else { return }
                if newReleasesModel.responseCode == StatusCodes.code200OK {
                    strongSelf.newReleasesPlaylistsModel = newReleasesModel.model
                }
            }
            .store(in: &cancelBag)
        
        getFeaturedPlaylists
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] featuredPlaylistsModel in
                guard let strongSelf = self else { return }
                if featuredPlaylistsModel.responseCode == StatusCodes.code200OK {
                    strongSelf.featuredPlaylistsModel = featuredPlaylistsModel.model
                }
            }
            .store(in: &cancelBag)
        
        let allDataZip = Publishers.Zip(getNewReleases,
                                        getFeaturedPlaylists)
        
        allDataZip
            .receive(on: RunLoop.main)
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] _, _ in
                self?.viewState = .show
            }
            .store(in: &cancelBag)
    }
}
