//
//  HomeView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import SwiftUI

struct HomeView: View {
    @StateObject private var viewModel = HomeViewModel()
    
    var body: some View {
        NavigationView {
            StateView(viewState: $viewModel.viewState,
                      upperBackgroundColor: .constant(.systemIndigo)) {
                VStack() {
                    ScrollView {
                        featuredPlaylistsView(items: viewModel.featuredPlaylistsModel.playlists.items,
                                              message: viewModel.featuredPlaylistsModel.message)
                        newReleasesView(items: viewModel.newReleasesPlaylistsModel.albums.items,
                                        message: "New releases")
                    }
                    Spacer()
                }
                .padding(10)
            }
            .navigationBarTitleDisplayMode(.inline)
        }
    }
    
    @ViewBuilder
    func newReleasesView(items: [AlbumNetworkModel],
                         message: String) -> some View {
        VStack(alignment: .leading) {
            Text(message)
                .modifier(ProjectTitleModifier())
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack(spacing: 15) {
                    ForEach(items, id: \.id) { item in
                        NavigationLink(destination: NavigationLazyView(
                            AlbumTracksView(albumId: item.id,
                                            playlistModel: item,
                                            releaseYear: item.releaseYear))
                        )
                        {
                            VStack {
                                StandardizedAsyncImage(imageURL: item.mainImage)
                                    .frame(width: 150, height: 150)
                                VStack {
                                    Text(item.artistsNames)
                                        .modifier(ProjectCalloutBoldModifier())
                                    Text(item.name)
                                        .modifier(ProjectCalloutModifier())
                                        .frame(width: 150)
                                    Spacer()
                                }
                                .frame(height: 50)
                            }
                        }
                    }
                    .frame(width: 150)
                }
            }
            Spacer()
        }
    }
    
    @ViewBuilder
    func featuredPlaylistsView(items: [ItemNetworkModel],
                               message: String) -> some View {
        VStack(alignment: .leading) {
            Text(message)
                .modifier(ProjectTitleModifier())
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack(spacing: 15) {
                    ForEach(items, id: \.id) { item in
                        NavigationLink(destination: NavigationLazyView(
                            PlaylistTracksView(playlistId: item.id,
                                               playlistModel: item))
                        ) {
                            VStack {
                                StandardizedAsyncImage(imageURL: item.mainImage)
                                    .frame(width: 150, height: 150)
                                VStack {
                                Text(item.description ?? "")
                                    .modifier(ProjectCalloutModifier())
                                    .frame(width: 150)
                                    Spacer()
                                }
                                .frame(height: 50)
                            }
                            
                        }
                    }
                    .frame(width: 150)
                }
            }
            Spacer()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
