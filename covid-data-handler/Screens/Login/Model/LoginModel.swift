//
//  LoginModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 12/02/2022.
//

import Foundation

struct LoginModel {
    let responseType = "response_type=code"
    let clientId = "client_id=8c3ea0c42a6c4b3d9339f43442630447"
    let scope = "scope=user-read-private%20user-read-email&playlist-read-private"
    let state = String.random(length: 16)
}
