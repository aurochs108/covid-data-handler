//
//  LoginViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 19/12/2021.
//

import Foundation
import Combine
import SwiftUI

class LoginViewModel: BaseViewModel {
    let loginCode = PassthroughSubject<String, Never>()
    let errorLoginMessage = PassthroughSubject<String, Never>()
    @Binding var appState: AppAuthAction
    private let authManager = AuthManager.shared
    private let loginModel = LoginModel()
    
    init(appState: Binding<AppAuthAction>) {
        self._appState = appState
        super.init()
        bindLoginCode()
        bindLoginErrorMessage()
    }
    
    func getLoginUrl() -> URL {
        let urlString = ProjectLinks.Login.LOGIN_URL + "?\(loginModel.responseType)&\(loginModel.clientId)&\(loginModel.scope)"
        + "&redirect_uri=\(ProjectLinks.Login.CALLBACK_URI)"
        + "&state=\(loginModel.state)"
        return URL(string: urlString)!
    }
    
    private func bindLoginCode() {
        loginCode
            .subscribe(on: RunLoop.main)
            .sink { [weak self] code in
                guard let strongSelf = self else { return }
                self?.authManager.getAccessToken(code: code,
                                                 completion: { result in
                    switch result {
                    case .success(let tokenModel):
                        TokenUtil.saveAccessToken(authToken: tokenModel.accessToken,
                                                  expiresInSeconds: tokenModel.expiresIn)
                        TokenUtil.saveRefreshToken(refreshToken: tokenModel.refreshToken)
                        strongSelf.appState = .showApp
                    case .failure(let error):
                        print(error)
                        strongSelf.appState = .error
                    }
                })
            }
            .store(in: &cancelBag)
    }
    
    private func bindLoginErrorMessage() {
        errorLoginMessage
            .subscribe(on: RunLoop.main)
            .sink { [weak self] error in
                print(error)
                self?.appState = .error
            }
            .store(in: &cancelBag)
    }
}
