//
//  LoginView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 19/12/2021.
//

import SwiftUI
import Combine

struct LoginView: View {
    @ObservedObject var viewModel: LoginViewModel
    
    init(appState: Binding<AppAuthAction>) {
        self.viewModel = .init(appState: appState)
    }
    
    var body: some View {
        LoginWebView(url: viewModel.getLoginUrl(),
                loginCode: viewModel.loginCode,
                errorMessage: viewModel.errorLoginMessage)
            .navigationBarTitle("")
            .navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
        }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(appState: .constant(.showLoginPage))
    }
}
