//
//  SwiftUIWebView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 19/12/2021.
//

import Foundation
import SwiftUI
import WebKit
import Combine

struct LoginWebView: UIViewRepresentable {
    let url: URL
    unowned var loginCode: PassthroughSubject<String, Never>
    unowned var errorMessage: PassthroughSubject<String, Never>
    
    func updateUIView(_ uiView: WKWebView, context: Context) { }
    
    func makeCoordinator() -> LoginWebView.Coordinator {
        Coordinator(self,
                    loginCode: loginCode,
                    errorMessage: errorMessage)
    }
    
    func makeUIView(context: Context) -> WKWebView {
        let view = WKWebView()
        view.navigationDelegate = context.coordinator
        view.load(URLRequest(url: url))
        return view
    }
    
    class Coordinator: NSObject, WKNavigationDelegate {
        let parent: LoginWebView
        unowned var loginCode: PassthroughSubject<String, Never>
        unowned var errorMessage: PassthroughSubject<String, Never>
        private let codeParameter = "code"
        private let errorParameter = "error"
        
        init(_ parent: LoginWebView,
             loginCode: PassthroughSubject<String, Never>,
             errorMessage: PassthroughSubject<String, Never>) {
            self.parent = parent
            self.loginCode = loginCode
            self.errorMessage = errorMessage
        }
        
        func webView(_ webView: WKWebView,
                     didFinish navigation: WKNavigation!) {
            do {
                try retrieveAndSendLoginCode(webView: webView)
            } catch {
                retrieveAndSendLoginError(webView: webView)
            }
        }
        
        private func retrieveAndSendLoginCode(webView: WKWebView) throws {
            guard let code = getQueryStringParameter(url: webView.url?.absoluteString,
                                                     parameter: codeParameter) else {
                throw LoginError.loginCodeNotRetrieved
            }
            loginCode.send(code)
        }
        
        private func retrieveAndSendLoginError(webView: WKWebView) {
            guard let error = getQueryStringParameter(url: webView.url?.absoluteString,
                                                      parameter: codeParameter) else { return }
            loginCode.send(error)
        }
        
        func getQueryStringParameter(url: String?,
                                     parameter: String) -> String? {
            guard let url = url,
                  let urlComponents = URLComponents(string: url) else { return nil }
            return urlComponents.queryItems?.first(where: { $0.name == parameter })?.value
        }
    }
}
