//
//  HomeCoordinator.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 18/12/2021.
//

import Foundation
import SwiftUI

enum TabBar: Hashable {
    case home
    case dashboard
    case profile
}

class TabBarViewModel: ObservableObject {
    @Published var tab = TabBar.home
}

