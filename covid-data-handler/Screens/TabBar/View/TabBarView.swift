//
//  TabBarView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 18/12/2021.
//

import SwiftUI

struct TabBarView: View {
    @StateObject private var viewModel = TabBarViewModel()
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        TabView(selection: $viewModel.tab) {
            HomeView()
                .tabItem { Label("Home", systemImage: "music.note.house.fill") }
                .tag(TabBar.home)
            
            ProfileView()
                .tabItem { Label("Profile", systemImage: "gear") }
                .tag(TabBar.profile)
            
            DashboardView()
                .tabItem { Label("Dashboard", systemImage: "rectangle.split.3x1.fill") }
                .tag(TabBar.dashboard)
        }
        .accentColor(colorScheme == .light ? .black : .white)
    }
}


struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
