//
//  AlbumTracksViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import Foundation
import Resolver

class AlbumTracksViewModel: TracksListViewModel {
    
    @Injected private var albumTracksNetworkRepository: AlbumTracksRepositoryProtocol
    private var playlistItemDbRepository: PlaylistItemDbRepository!
    
    private let albumId: String
    var albumItemsModels: [AlbumTrackNetworkModel.Item] = []
    let albumModel: PlaylistModelProtocol
    let releaseYear: String?
    
    @Published var filteredItemsModels: [AlbumTrackNetworkModel.Item] = []
    
    init(albumId: String,
         albumModel: PlaylistModelProtocol,
         releaseYear: String?) {
        self.albumId = albumId
        self.albumModel = albumModel
        self.releaseYear = releaseYear
        super.init()
        getAlbumsItems(albumId)
    }
    
    func filterPlaylistItems(input: String) {
        self.filteredItemsModels = filterItems(input: input,
                                               filteringTarget: self.albumItemsModels)
    }
    
    private func getAlbumsItems(_ playlistId: String) {
        guard Connectivity.isConnectedToInternet else {
            viewState = .noInternet
            return
        }
        
        albumTracksNetworkRepository.getAlbumTracks(albumId: playlistId)
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] responseModel in
                guard let strongSelf = self else { return }
                guard responseModel.responseCode == StatusCodes.code200OK else {
                    strongSelf.viewState = .error
                    return
                }
                strongSelf.albumItemsModels = responseModel.model?.items ?? []
                strongSelf.filteredItemsModels = strongSelf.albumItemsModels
                strongSelf.viewState = .show
            }
            .store(in: &cancelBag)
    }
}
