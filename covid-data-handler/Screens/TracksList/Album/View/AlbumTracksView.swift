//
//  AlbumTracksView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct AlbumTracksView: View {
    @ObservedObject private var viewModel: AlbumTracksViewModel
    
    @State private var upperBackgroundColor: UIColor = .black
    @State private var scrollOffset: CGFloat = 0.0
    @State private var searchBarInput = ""
    
    private let initialScrollOffset = 200.0
    
    init(albumId: String,
         playlistModel: PlaylistModelProtocol,
         releaseYear: String?) {
        self.viewModel = .init(albumId: albumId,
                               albumModel: playlistModel,
                               releaseYear: releaseYear)
    }
    
    var body: some View {
        StateView(viewState: $viewModel.viewState,
                  upperBackgroundColor: $upperBackgroundColor) {
            VStack {
                PlaylistImageView(upperBackgroundColor: $upperBackgroundColor,
                                  scrollOffset: $scrollOffset,
                                  image: viewModel.albumModel.mainImage)
                Spacer()
            }
            ScrollViewWithPositionReader(offset: self.$scrollOffset) {
                scrolledContent()
            }
            .searchable(text: $searchBarInput)
            .onChange(of: searchBarInput) { searchInput in
                viewModel.filterPlaylistItems(input: searchInput)
            }
        }
    }
    
    @ViewBuilder
    private func scrolledContent() -> some View {
            VStack {
                Spacer()
                    .frame(height: initialScrollOffset)
                    
                headline()
                    
                HStack {
                    VStack(alignment: .leading) {
                        PlaylistSmallInfo(artistsNames: viewModel.albumModel.artistsNames,
                                          leftInfo: viewModel.albumModel.type.rawValue,
                                          rightInfo: viewModel.releaseYear)
                        TracksListActionsButtons()
                    }
                    Spacer()
                    PlayActionButtons(shouldShowShuffle: false)
                }
                albumItemsList()
            }
            .padding()
    }
    
    @ViewBuilder
    private func albumItemsList() -> some View {
        LazyVStack {
            ForEach(viewModel.filteredItemsModels, id: \.self) { track in
                PlaylistTrackCell(image: viewModel.albumModel.mainImage,
                                  trackName: track.name,
                                  artistsNames: track.artistsNames)
            }
        }
    }
    
    @ViewBuilder
    private func headline() -> some View {
        VStack(spacing: 10) {
            HStack {
                Text(viewModel.albumModel.name)
                    .modifier(ProjectTitleModifier())
                Spacer()
            }
        }
    }
}

//struct AlbumTracksView_Previews: PreviewProvider {
//    static var previews: some View {
//        AlbumTracksView()
//    }
//}
