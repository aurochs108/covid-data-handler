//
//  PlaylistView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/01/2022.
//

import SwiftUI

struct PlaylistTracksView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @ObservedObject private var viewModel: PlaylistViewModel
    
    @State private var upperBackgroundColor: UIColor = .black
    @State private var scrollOffset: CGFloat = 0.0
    @State private var searchBarInput = ""
    
    private let initialScrollOffset = 200.0
    
    init(playlistId: String,
         playlistModel: PlaylistModelProtocol) {
        self.viewModel = .init(playlistId: playlistId,
                               playlistModel: playlistModel)
    }
    
    var body: some View {
        StateView(viewState: $viewModel.viewState,
                  upperBackgroundColor: $upperBackgroundColor) {
            VStack {
                PlaylistImageView(upperBackgroundColor: $upperBackgroundColor,
                                  scrollOffset: $scrollOffset,
                                  image: viewModel.playlistModel.mainImage)
                Spacer()
            }
            ScrollViewWithPositionReader(offset: self.$scrollOffset) {
                scrolledContent()
            }
            .searchable(text: $searchBarInput)
            .onChange(of: searchBarInput) { searchInput in
                viewModel.filterPlaylistItems(input: searchInput)
            }
        }
    }
    
    @ViewBuilder
    private func scrolledContent() -> some View {
            VStack {
                Spacer()
                    .frame(height: initialScrollOffset)
                    
                headline()
                    
                HStack {
                    VStack(alignment: .leading) {
                        PlaylistSmallInfo(artistsNames: viewModel.playlistModel.artistsNames,
                                          leftInfo: "65 Likes",
                                          rightInfo: "90h")
                        TracksListActionsButtons()
                    }
                    Spacer()
                    PlayActionButtons()
                }
                playlistsItemsList()
            }
            .padding()
    }
    
    
    
    @ViewBuilder
    private func playlistsItemsList() -> some View {
        LazyVStack {
            ForEach(viewModel.filteredPlaylists, id: \.self) {
                PlaylistTrackCell(image: $0.mainImage,
                                  trackName: $0.name,
                                  artistsNames: $0.artistsNames) }
        }
    }
    
    @ViewBuilder
    private func headline() -> some View {
        VStack(alignment: .center, spacing: 10) {
            Text(viewModel.playlistModel.name)
                .modifier(ProjectTitleModifier())
            
            Text(viewModel.playlistModel.description ?? "")
                .modifier(ProjectCalloutModifier())
        }
    }
}

//struct PlaylistView_Previews: PreviewProvider {
//    static var previews: some View {
//        PlaylistView(albumId: "")
//    }
//}
