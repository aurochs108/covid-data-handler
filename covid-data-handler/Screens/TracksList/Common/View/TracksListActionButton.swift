//
//  ActionButton.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct TracksListActionButton: View {
    let imageName: String
    let imageColor: Color
    let action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            SvgImage(imageName: imageName,
                     imageColor: imageColor)
        }
    }
}
