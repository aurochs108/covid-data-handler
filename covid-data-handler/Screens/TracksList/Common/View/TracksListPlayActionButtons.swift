//
//  PlayActionButtons.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct PlayActionButtons: View {
    var shouldShowShuffle: Bool = true
    
    var body: some View {
        ZStack {
            TracksListActionButton(imageName: "Play",
                         imageColor: .green) {
                print("Play")
            }
            .frame(width: 50, height: 50)
            
            if shouldShowShuffle {
                TracksListActionButton(imageName: "Shuffle",
                                       imageColor: .white) {
                    print("Shuffle")
                }
                .frame(width: 20, height: 20)
                .offset(x: 20, y: 20)
            }
        }
    }
}

struct PlayActionButtons_Previews: PreviewProvider {
    static var previews: some View {
        PlayActionButtons()
    }
}
