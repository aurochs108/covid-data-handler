//
//  PlaylistSmallInfo.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct PlaylistSmallInfo: View {
    let artistsNames: String
    let leftInfo: String
    let rightInfo: String?
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                StandardizedAsyncImage(imageURL: URL(string: "not exist"))
                    .frame(width: 20, height: 20)
                    .clipShape(Circle())
                Text(artistsNames)
                    .font(.caption.bold())
                    .foregroundColor(.white)
            }
            HStack {
                Text(leftInfo.firstUppercased)
                    .foregroundColor(.white)
                    .font(.caption)
                
                if let rightInfo = rightInfo {
                    Text("•")
                        .foregroundColor(.white)
                        .font(.caption)
                    Text(rightInfo)
                        .foregroundColor(.white)
                        .font(.caption)
                }
            }
        }
    }
}

struct PlaylistSmallInfo_Previews: PreviewProvider {
    static var previews: some View {
        PlaylistSmallInfo(artistsNames: PreviewStatics.randomTitle,
                          leftInfo: "999 Likes",
                          rightInfo: "999h")
    }
}
