//
//  PlaylistHeadlineView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct PlaylistImageView: View {
    @Binding var upperBackgroundColor: UIColor
    @Binding var scrollOffset: CGFloat
    let image: URL?
    
    private static let imageDefaultSideDimension = 200.0
    @State private var imageSideDimension = PlaylistImageView.imageDefaultSideDimension
    @State private var imageOpacity = 1.0
    
    
    var body: some View {
        MainStandardizedAsyncImage(imageURL: image,
                                   averageColor: $upperBackgroundColor)
            .frame(width: self.imageSideDimension,
                   height: self.imageSideDimension)
            .opacity(self.imageOpacity)
            .onChange(of: self.scrollOffset) { offset in
                setupImageOpacity(offset: offset)
                setupImageSideDimension(offset: offset)
            }
    }
    
    //MARK: - Functions
    func setupImageOpacity(offset: CGFloat) {
        let countedImageOpacity = getImageOpacity(scrollOffset: offset)
        guard countedImageOpacity != self.imageOpacity else { return }
        self.imageOpacity = countedImageOpacity
    }
    
    func setupImageSideDimension(offset: CGFloat) {
        let countedImageSideDimension = getImageSideDimension(scrollOffset: offset)
        guard countedImageSideDimension != self.imageSideDimension else { return }
        self.imageSideDimension = countedImageSideDimension
    }
    
    private func getImageSideDimension(scrollOffset: CGFloat) -> Double {
        guard scrollOffset < 200 else { return .zero }
        guard scrollOffset > 0 else { return PlaylistImageView.imageDefaultSideDimension }
        
        let sideDimension = PlaylistImageView.imageDefaultSideDimension - (scrollOffset * 0.5)
        return sideDimension
    }
    
    private func getImageOpacity(scrollOffset: CGFloat) -> Double {
        guard scrollOffset < 200 else { return .zero }
        guard scrollOffset > 0 else { return 1 }
        
        let opaqueValue = (100 - (scrollOffset * 0.5)) / 100
        return opaqueValue
    }
}

struct PlaylistImageView_Previews: PreviewProvider {
    static var previews: some View {
        PlaylistImageView(upperBackgroundColor: .constant(.green),
                          scrollOffset: .constant(50.0),
                          image: PreviewStatics.randomImageURL)
    }
}
