//
//  ProfileView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 29/12/2021.
//

import SwiftUI


struct ProfileView: View {
    @StateObject var viewModel = ProfileViewModel()
    
    @State var averageColor: UIColor = .black
    
    private let buttonWidth: CGFloat = 100
    private let playlistCellHeight: CGFloat = 60
    private let maxNumberOfPlaylistsToShow = 3
    
    var body: some View {
        NavigationView {
            StateView(viewState: $viewModel.viewState,
                      upperBackgroundColor: $averageColor) {
                VStack {
                    profileImageView(imageUrl: viewModel.getProfileImageUrl())
                    Text(viewModel.profileModel.displayName ?? "")
                        .modifier(ProjectTitleModifier())
                    
                    editProfileButton()
                    logoutButton()
                    
                    Spacer()
                        .frame(height: 40)
                    playlistAndFollowersInfo()
                    Spacer()
                        .frame(height: 10)
                    Text("Playlists")
                        .font(.title.weight(.medium))
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    playlistsList()
                }
            }
            .navigationBarTitleDisplayMode(.inline)
        }
    }
    
    @ViewBuilder
    private func profileImageView(imageUrl: URL?) -> some View {
        MainStandardizedAsyncImage(imageURL: imageUrl!,
                                   averageColor: $averageColor)
            .frame(width: 200, height: 200)
            .clipShape(Circle())
    }

    @ViewBuilder
    private func editProfileButton() -> some View {
        RoundedButton(buttonTitle: "Edit profile",
                      buttonWidth: buttonWidth) {
            editProfileButtonTapped()
        }
    }
    
    @ViewBuilder
    private func logoutButton() -> some View {
        
        RoundedButton(buttonTitle: "Logout",
                      buttonWidth: buttonWidth) {
            logoutTapped()
        }
    }
    
    @ViewBuilder
    private func playlistAndFollowersInfo() -> some View {
        HStack {
            basicInfoSpacer()
            basicInfoView(count: viewModel.playlistModel.items.count,
                          title: "PLAYLISTS")
            Spacer()
            basicInfoView(count: viewModel.profileModel.followers.total,
                          title: "FOLLOWERS")
            Spacer()
            basicInfoView(count: 999, title: "FOLLOWING")
            basicInfoSpacer()
        }
    }
    
    
    @ViewBuilder
    private func playlistsList() -> some View {
        ScrollView {
            LazyVStack {
                ForEach(viewModel.getRangeOfSeeablePlaylists(maxNumberOfPlaylistsToShow)) { index in
                    NavigationLink(destination: NavigationLazyView(
                        PlaylistTracksView(playlistId: viewModel.getPlaylistId(playlistIndex: index),
                                     playlistModel: viewModel.playlistModel.items[index]))
                    ) {
                        PlaylistCell(title: viewModel.getPlaylistName(playlistIndex: index),
                                     playlistImageUrl: viewModel.getImageOfPlaylist(playlistIndex: index),
                                     cellHeight: playlistCellHeight)
                    }
                }
                if viewModel.isShowMorePlaylistRequired(maxNumberOfPlaylistsToShow) {
                    NavigationLink(destination: MorePlaylistsView(playlistItemsModel: viewModel.playlistModel.items,
                                                                  playlistCellHeight: playlistCellHeight)) {
                        seeMorePlaylistCell(cellHeight: playlistCellHeight)
                    }
                }
            }
        }
    }
    
    @ViewBuilder
    private func seeMorePlaylistCell(cellHeight: CGFloat) -> some View {
        HStack {
            Text("See more playlists")
                .foregroundColor(.white)
                .font(.headline.bold())
                .lineLimit(.zero)
            Spacer()
            Image(systemName: "chevron.right")
                .foregroundColor(.white)
        }
        .frame(height: cellHeight)
    }
    
    @ViewBuilder
    private func basicInfoSpacer() -> some View {
        Spacer()
            .frame(width: 20)
    }
    
    @ViewBuilder
    private func basicInfoView(count: Int, title: String) -> some View {
        VStack(alignment: .center) {
            Text("\(count)")
                .foregroundColor(.white)
                .font(.caption.weight(.medium))
            Text(title)
                .foregroundColor(.gray)
                .font(.caption.weight(.thin))
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
