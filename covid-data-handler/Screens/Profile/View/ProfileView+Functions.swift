//
//  ProfileView+Functions.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation

extension ProfileView {
    func editProfileButtonTapped() {
        print("edit profile") // show modal
    }
    
    func logoutTapped() {
        viewModel.logout()
    }
}
