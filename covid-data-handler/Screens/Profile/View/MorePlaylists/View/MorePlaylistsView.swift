//
//  MorePlaylistsView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 09/01/2022.
//

import SwiftUI

struct MorePlaylistsView: View {
    let playlistItemsModel: [ItemNetworkModel]
    let playlistCellHeight: CGFloat
    
    var body: some View {
            ZStack {
                Color.black
                    .ignoresSafeArea()
                ScrollView {
                    LazyVStack {
                        ForEach(playlistItemsModel, id: \.self) { item in
                            NavigationLink(destination: NavigationLazyView(
                                PlaylistTracksView(playlistId: item.id,
                                             playlistModel: item))
                            ) {
                                PlaylistCell(title: item.name,
                                             playlistImageUrl: item.mainImage,
                                             cellHeight: playlistCellHeight)
                            }
                        }
                    }
                }
            }
    }
}

//struct PlaylistsView_Previews: PreviewProvider {
//    static var previews: some View {
//        PlaylistsView()
//    }
//}
