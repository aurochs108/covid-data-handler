//
//  ProfileViewModel+NetworkRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation
import Combine

extension ProfileViewModel {
    func bindNetworkRepository() {
        let getCurrentUserProfile = profileNetworkRepository.getCurrentUserProfile()
        let getCurrentUserPlaylist = currentUserPlaylistNetworkRepository.getCurrentUserPlaylists()
        
        getCurrentUserProfile
            .sink { completion in
                print(completion) //add error catching and some retry!
            } receiveValue: { [weak self] profileModel in
                guard let strongSelf = self else { return }
                if profileModel.responseCode == StatusCodes.code200OK {
                    strongSelf.profileModel = profileModel.model
                }
            }
            .store(in: &cancelBag)
        
        getCurrentUserPlaylist
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] playlistModel in
                guard let strongSelf = self else { return }
                if playlistModel.responseCode == StatusCodes.code200OK {
                    strongSelf.playlistModel = playlistModel.model
                }
            }
            .store(in: &cancelBag)
        
        let allDataZip = Publishers.Zip(getCurrentUserProfile,
                                        getCurrentUserPlaylist)
        
        allDataZip
            .receive(on: RunLoop.main)
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] _, _ in
                self?.viewState = .show
            }
            .store(in: &cancelBag)
    }
}
