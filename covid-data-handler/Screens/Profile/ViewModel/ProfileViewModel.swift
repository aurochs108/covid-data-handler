//
//  ProfileViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 29/12/2021.
//

import Foundation
import Combine
import Resolver

class ProfileViewModel: BaseViewModel {
    let authManager = AuthManager.shared
    
    @Injected var profileNetworkRepository: ProfileRepositoryProtocol
    @Injected var currentUserPlaylistNetworkRepository: PlaylistsRepositoryProtocol
    
    var profileModel: ProfileNetworkModel!
    var playlistModel: PlaylistsNetworkModel!
    
    override init() {
        super.init()
        bindNetworkRepository()
    }
    
    func getPlaylistId(playlistIndex: Int) -> String {
        playlistModel.items[playlistIndex].id
    }
    
    func getPlaylistName(playlistIndex: Int) -> String {
        playlistModel.items[playlistIndex].name
    }
    
    func getImageOfPlaylist(playlistIndex: Int) -> URL? {
        playlistModel.items[playlistIndex].mainImage
    }
    
    func getRangeOfSeeablePlaylists(_ maxNumberOfPlaylistsToShow: Int) -> Range<Int> {
        let numberOfPlaylists = playlistModel.items.count
        guard numberOfPlaylists > maxNumberOfPlaylistsToShow else { return 0..<numberOfPlaylists }
        return 0..<maxNumberOfPlaylistsToShow
    }
    
    func isShowMorePlaylistRequired(_ maxNumberOfPlaylistsToShow: Int) -> Bool {
        let numberOfPlaylists = playlistModel.items.count
        return numberOfPlaylists > maxNumberOfPlaylistsToShow
    }
    
    func getProfileImageUrl() -> URL? {
        guard let imageUrl = profileModel?.images.first else { return nil }
        return imageUrl.url
    }
    
    func logout() {
        authManager.logout()
    }
}
