//
//  TotalData.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/12/2021.
//

import Foundation

struct TotalData: Codable, Hashable {
    let confirmed: Int
    let recovered: Int
    let critical: Int
    let deaths: Int
    let active: Int
    let date: String
}
