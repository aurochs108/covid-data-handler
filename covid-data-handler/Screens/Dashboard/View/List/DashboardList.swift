//
//  SwiftUIView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/12/2021.
//

import SwiftUI

struct DashboardList: View {
    @Environment(\.viewActions) var viewActions
    @Binding var totalDataModels: [TotalData]
    
    var body: some View {
        List {
            
        }
        .listStyle(InsetListStyle())
        .refreshable {
            viewActions.send(.reload)
        }
    }
}

//struct DashboardList_Previews: PreviewProvider {
//    static var previews: some View {
//        DashboardList()
//    }
//}
