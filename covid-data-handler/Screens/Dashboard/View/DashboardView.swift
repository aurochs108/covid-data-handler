//
//  ContentView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 11/12/2021.
//

import SwiftUI

struct DashboardView: View {
    @StateObject var viewModel = DashboardViewModel()
    var menuItemsColumsLayout: [GridItem]
    let menuItemsArray: [MenuItemModel]
    @State var menuItemPosition: Double = 1.0
    
    init() {
        self.menuItemsArray = [.init(title: "Total data", position: 1),
                               .init(title: "Test", position: 2),
                               .init(title: "AAAa", position: 3)]
        self.menuItemsColumsLayout = Array(repeating: .init(.flexible()),
                                           count: menuItemsArray.count)
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack(spacing: 0) {
                    MenuNavigationView(menuItemsArray: menuItemsArray,
                                       menuItemPosition: $menuItemPosition)
                        .frame(height: 31)
                    ContentView(viewModel: viewModel,
                                menuItemPosition: $menuItemPosition)
                }
            }
            .environment(\.viewActions, viewModel.dataAction)
            .navigationBarTitle("Dashboard")
        }
    }
}


struct ContentView: View {
    @ObservedObject var viewModel: DashboardViewModel
    let array = [Color.red,
                 Color.green,
                 Color.yellow]
    @Binding var menuItemPosition: Double
    
    var body: some View {
        TabView(selection: $menuItemPosition) {
            Color.red
                .tag(1.0)
            Color.green
                .tag(2.0)
            Color.yellow
                .tag(3.0)
        }
        .tabViewStyle(.page(indexDisplayMode: .never))
    }
}


struct MenuNavigationView: View {
    @Binding var menuItemPosition: Double
    let menuItemsArray: [MenuItemModel]
    var menuItemsColumsLayout: [GridItem]
    
    init(menuItemsArray: [MenuItemModel], menuItemPosition: Binding<Double>) {
        self.menuItemsArray = menuItemsArray
        self._menuItemPosition = menuItemPosition
        self.menuItemsColumsLayout = Array(repeating: .init(.flexible()), count: menuItemsArray.count)
    }
    
    var body: some View {
        VStack {
            LazyVGrid(columns: menuItemsColumsLayout) {
                ForEach(menuItemsArray, id: \.self) { menuItem in
                    MenuItemView(model: menuItem, menuItemPosition: $menuItemPosition)
                }
            }
            MenuNavigationBottomLineView(menuItemPosition: $menuItemPosition,
                                         menuItemsArray: menuItemsArray)
                .animation(.easeInOut, value: menuItemPosition)
        }
    }
}

struct MenuNavigationBottomLineView: View {
    @Binding var menuItemPosition: Double
    @State var lineWidth = 100.0 ///TODO add width for meniu item text - not fixed size!
    let menuItemsArray: [MenuItemModel]
    
    private func lineCenterPointPosition(for metrics: GeometryProxy,
                                         _ menuItemPosition: Double,
                                         _ menuItemsArray: [MenuItemModel]) -> Double {
        let elementsInMenu = Double(menuItemsArray.count)
        let centerOfView = metrics.size.width / 2
        let lineHalfWidth = lineWidth / 2
        let widthOfMenuItem = metrics.size.width / elementsInMenu
        let centerElementOfMenu = (elementsInMenu / 2) + 0.5
        guard menuItemPosition != centerElementOfMenu else { return centerOfView - lineHalfWidth }
        if menuItemPosition < centerElementOfMenu {
            let elementsToOffset = centerElementOfMenu - menuItemPosition
            return (centerOfView - lineHalfWidth) - elementsToOffset * widthOfMenuItem
        } else {
            let elementsToOffset = menuItemPosition - centerElementOfMenu
            return (centerOfView - lineHalfWidth) + elementsToOffset * widthOfMenuItem
        }
    }
    
    var body: some View {
        GeometryReader { metrics in
            Color.white
                .frame(width: lineWidth, height: 2, alignment: .center)
                .offset(x: lineCenterPointPosition(for: metrics,
                                                      menuItemPosition,
                                                      menuItemsArray))
        }
    }
}

struct MenuItemView: View {
    let model: MenuItemModel
    @Binding var menuItemPosition: Double
    
    var body: some View {
        Button {
            withAnimation(.default) {
                menuItemPosition = Double(model.position)
            }
        } label: {
            Text(model.title)
                .foregroundColor(.white)
                .font(.headline)
                .fontWeight(Int(menuItemPosition) == model.position ? .bold : .light)
        }
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}
