//
//  SplashScreenView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 26/12/2021.
//

import SwiftUI

struct MasterView: View {
    @StateObject var viewModel = MasterViewModel()
    
    var body: some View {
        if Connectivity.isConnectedToInternet {
            switch viewModel.appState {
            case .showApp: TabBarView()
            case .showLoginPage: LoginView(appState: $viewModel.appState)
            case .loading, .refreshAccessToken: LaunchScreenView()
            case .error: EmptyView() ///change this
            }
        } else {
            NoInternetConnectionView()
        }
    }
}

struct MasterView_Previews: PreviewProvider {
    static var previews: some View {
        MasterView()
    }
}
