//
//  WebKitHelper.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/02/2022.
//

import WebKit

class WebKitHelper {
    
    static func clearAllCookies() {
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes,
                                                        for: [record],
                                                        completionHandler: {})
            }
        }
    }
}
