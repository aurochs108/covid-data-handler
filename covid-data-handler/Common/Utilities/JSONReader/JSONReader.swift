//
//  JSONReader.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 11/03/2022.
//

import Foundation

class JSONReader {
    static func loadFromJson<MODEL: Codable>(fileName: String) throws -> MODEL? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "json") else { throw JSONReaderError.invalidFileName }
        guard let data = try? Data(contentsOf: url),
              let model = try? decoder.decode(MODEL.self, from: data) else { throw JSONReaderError.invalidDecodeModel }
        return model
    }
}
