//
//  Connectivity.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 09/01/2022.
//

import Alamofire

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    
    static var isConnectedToInternet: Bool {
        return self.sharedInstance.isReachable
    }
}
