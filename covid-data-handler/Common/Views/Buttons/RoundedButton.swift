//
//  RoundedButton.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 26/01/2022.
//

import SwiftUI

struct RoundedButton: View {
    let buttonTitle: String
    var buttonHeight: CGFloat = 30
    var buttonWidth: CGFloat? = nil
    var backgroundColor: Color = .gray
    var foregroundColor: Color = .white
    let action: () -> Void
    
    var body: some View {
        Button(buttonTitle) { action() }
        .padding()
        .frame(height: buttonHeight)
        .frame(width: buttonWidth)
        .foregroundColor(.white)
        .font(.caption)
        .background(backgroundColor.opacity(0.2))
        .clipShape(Capsule())
    }
}


struct RoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedButton(buttonTitle: PreviewStatics.randomTitle) {
            print(PreviewStatics.randomActionText)
        }
    }
}
