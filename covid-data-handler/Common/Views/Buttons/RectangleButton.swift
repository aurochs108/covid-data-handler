//
//  SortButton.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 25/01/2022.
//

import SwiftUI

struct RectangleButton: View {
    @Binding var color: UIColor
    let buttonTitle: String
    var buttonHeight: CGFloat = 30
    var buttonWidth: CGFloat? = .infinity
    var foregroundColor: Color = .white
    let action: () -> Void
    
    var body: some View {
        Button(buttonTitle) { action() }
        .padding()
        .frame(height: buttonHeight)
        .frame(maxWidth: buttonWidth)
        .foregroundColor(.white)
        .font(.caption)
        .background(Color(color).opacity(0.8))
        .cornerRadius(3)
    }
}

struct SortButton_Previews: PreviewProvider {
    static var previews: some View {
        RectangleButton(color: .constant(.blue),
                   buttonTitle: PreviewStatics.randomTitle) {
            print(PreviewStatics.randomActionText)
        }
    }
}
