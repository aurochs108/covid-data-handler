//
//  SvgImage.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 20/01/2022.
//

import SwiftUI

struct SvgImage: View {
    let imageName: String
    var imageColor: Color? = nil
    
    var body: some View {
        VStack {
          ZStack {
              Image(imageName)
                  .resizable()
              if imageColor != nil {
                  imageColor.blendMode(.sourceAtop)
              }
          }
          .drawingGroup(opaque: false)
        }
    }
}

struct SvgImage_Previews: PreviewProvider {
    static var previews: some View {
        SvgImage(imageName: "Heart",
                 imageColor: .red)
    }
}

