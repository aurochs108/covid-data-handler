//
//  StandardizedAsyncImage.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 09/01/2022.
//

import SwiftUI
import UIKit
import Kingfisher

struct StandardizedAsyncImage: View {
    let imageURL: URL?
    let contentMode: SwiftUI.ContentMode
    
    init(imageURL: URL?,
         contentMode: SwiftUI.ContentMode = .fit) {
        self.imageURL = imageURL
        self.contentMode = contentMode
    }
    
    var body: some View {
        KFImage(imageURL)
            .placeholder {
                Color.gray
                    .opacity(0.3)
            }
            .retry(maxCount: 3,
                   interval: .seconds(5))
            .resizable()
            .aspectRatio(contentMode: contentMode)
    }
}

struct MainStandardizedAsyncImage: View {
    let imageURL: URL?
    let contentMode: SwiftUI.ContentMode
    @Binding var averageColor: UIColor
    
    init(imageURL: URL?,
         contentMode: SwiftUI.ContentMode = .fit,
         averageColor: Binding<UIColor> = .constant(.black)) {
        self.imageURL = imageURL
        self.contentMode = contentMode
        self._averageColor = averageColor
    }
    
    var body: some View {
        KFImage(imageURL)
            .placeholder {
                Color.gray
                    .opacity(0.3)
            }
            .onSuccess({ imageResult in
                guard let averageColor = ImageHelper.getAverageColor(image: imageResult.image) else { return }
                self.averageColor = averageColor
            })
            .retry(maxCount: 3,
                   interval: .seconds(2))
            .resizable()
            .aspectRatio(contentMode: contentMode)
    }
}

struct StandaraizedAsyncImage_Previews: PreviewProvider {
    static var previews: some View {
        StandardizedAsyncImage(imageURL: PreviewStatics.randomImageURL)
    }
}
