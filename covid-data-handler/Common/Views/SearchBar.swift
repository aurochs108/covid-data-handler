//
//  SearchBar.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 25/01/2022.
//

import SwiftUI

struct SearchBar: View {
    @Binding var searchBarInput: String
    @Binding var searchBarColor: UIColor
    @State private var isEditing = false
    let emptySearchBarTitle: String
    
    var body: some View {
        TextField("", text: $searchBarInput)
            .modifier(PlaceholderStyle(showPlaceHolder: searchBarInput.isEmpty,
                                       placeholder: emptySearchBarTitle,
                                       color: .white))
            .foregroundColor(.white)
            .font(.caption)
            .padding(.leading, 7)
            .frame(height: 30)
            .padding(.horizontal, 25)
            .background(Color(searchBarColor).opacity(0.8))
            .cornerRadius(3)
            .overlay(imagesOverlay())
            .onTapGesture { self.isEditing = true }
    }
    
    @ViewBuilder
    private func imagesOverlay() -> some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .foregroundColor(.white)
                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 8)
            
            if isEditing {
                Button(action: {
                    self.isEditing = false
                    self.searchBarInput = ""
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                }) {
                    Image(systemName: "multiply.circle.fill")
                        .foregroundColor(.white)
                        .padding(.trailing, 8)
                }
            }
        }
    }
    
    struct PlaceholderStyle: ViewModifier {
        var showPlaceHolder: Bool
        var placeholder: String
        var color: Color

        public func body(content: Content) -> some View {
            ZStack(alignment: .leading) {
                if showPlaceHolder {
                    Text(placeholder)
                        .font(.caption)
                        .foregroundColor(color.opacity(0.8))
                }
                content
            }
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar(searchBarInput: .constant(""),
                  searchBarColor: .constant(.gray),
                  emptySearchBarTitle: "Find something")
    }
}
