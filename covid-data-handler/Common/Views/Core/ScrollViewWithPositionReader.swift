//
//  ScrollViewWithPositionReader.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 27/01/2022.
//

import SwiftUI

struct ScrollViewWithPositionReader<Content: View>: View {
    @Binding var offset: CGFloat
    var axes: Axis.Set = .vertical
    var showIndicators: Bool = true
    let contentView: () -> Content
    
    var body: some View {
        ScrollView(axes, showsIndicators: showIndicators) {
            contentView()
            .background(
                GeometryReader { geo in
                Color.clear.preference(key: ViewOffsetKey.self,
                                       value: -geo.frame(in: .named(ParameterKeyStatic.viewOffsetKey)).origin.y)
            })
            .onPreferenceChange(ViewOffsetKey.self) { self.offset = $0 }
        }
        .coordinateSpace(name: ParameterKeyStatic.viewOffsetKey)
    }
}
