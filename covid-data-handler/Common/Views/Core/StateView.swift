//
//  StateView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 22/12/2021.
//

import SwiftUI

enum ViewState {
    case loading
    case error
    case noInternet
    case empty
    case show
}

struct StateView<Content: View>: View {
    @Binding var viewState: ViewState
    @Binding private var upperBackgroundColor: UIColor
    private let contentView: () -> Content
    
    init(viewState: Binding<ViewState>,
         upperBackgroundColor: Binding<UIColor>,
         @ViewBuilder contentView: @escaping () -> Content) {
        self._viewState = viewState
        self._upperBackgroundColor = upperBackgroundColor
        self.contentView = contentView
    }
    
    var body: some View {
        switch viewState {
        case .loading: LoadingView()
        case .error: ErrorView()
        case .empty: NotFilledView()
        case .show: showView()
        case .noInternet: NoInternetView()
        }
    }
    
    @ViewBuilder
    func showView() -> some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [Color(upperBackgroundColor), .black, .black]),
                           startPoint: .top,
                           endPoint: .bottom)
                .ignoresSafeArea()
            contentView()
        }
    }
    
    @ViewBuilder
    func NoInternetView() -> some View {
        Text("No internet connection")
    }
    
    @ViewBuilder
    func LoadingView() -> some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            ProgressView()
        }
    }
    
    @ViewBuilder
    func ErrorView() -> some View {
        Text("Error occured.")
    }
    
    @ViewBuilder
    func NotFilledView() -> some View {
        Text("Nothing to see here.")
    }
}

struct StateView_Previews: PreviewProvider {
    static var previews: some View {
        StateView(viewState: .constant(.show),
                  upperBackgroundColor: .constant(.blue)) {
            Text("Hello world!")
        }
    }
}
