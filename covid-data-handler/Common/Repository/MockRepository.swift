//
//  MockRepository.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 11/03/2022.
//

import Foundation
import Combine

class MockRepository {
    func getMockDataRequest<MODEL: Codable>(fileName: String) -> AnyPublisher<ResponseModel<MODEL>, Error> {
        return Future { promise in
            do {
                let model: MODEL? = try JSONReader.loadFromJson(fileName: fileName)
                guard let model = model else { return promise(.failure(JSONReaderError.invalidDecodeModel)) }
                promise(.success(.init(model: model, responseCode: StatusCodes.code200OK)))
            } catch let error {
                print(error.localizedDescription)
                return promise(.failure(error))
            }
        }.eraseToAnyPublisher()
    }
}
