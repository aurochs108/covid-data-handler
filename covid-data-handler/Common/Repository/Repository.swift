//
//  Repository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 13/02/2022.
//

import Foundation
import Combine
import Alamofire

protocol Repository {
    func getModel<MODEL: Codable>(url: String) -> AnyPublisher<ResponseModel<MODEL>, Error>
    
    func getModel<MODEL: Codable,
                  PARAMETERS: Codable>(url: String,
                                       parameters: PARAMETERS) -> AnyPublisher<ResponseModel<MODEL>, Error>
}
