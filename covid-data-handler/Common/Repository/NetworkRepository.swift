//
//  NetworkRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation
import Combine
import Alamofire

class NetworkRepository: Repository {
    
    private static let sessionManager: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.timeoutIntervalForRequest = 30
        return Session(configuration: configuration,
                       interceptor: ProjectRequestInterceptor())
    }()
    
    func getModel<MODEL: Codable>(url: String) -> AnyPublisher<ResponseModel<MODEL>, Error> {
        getNetworkDataRequest(url: url, parameters: EmptyParameters())
    }
    
    func getModel<MODEL: Codable,
                  PARAMETERS: Codable>(url: String,
                                       parameters: PARAMETERS) -> AnyPublisher<ResponseModel<MODEL>, Error> {
        getNetworkDataRequest(url: url, parameters: parameters)
    }
    
    private func getNetworkDataRequest<MODEL: Codable,
                                       PARAMETERS: Codable>(url: String,
                                                            parameters: PARAMETERS) -> AnyPublisher<ResponseModel<MODEL>, Error> {
        return Future { promise in
            NetworkRepository.sessionManager.request(url,
                                                     method: .get,
                                                     parameters: parameters)
                .validate(statusCode: 200..<400)
                .responseData { response in
                    switch response.response?.statusCode {
                    case StatusCodes.code200OK.rawValue:
                        switch response.result {
                        case .success(let data):
                            let decoder = JSONDecoder()
                            decoder.dateDecodingStrategy = .iso8601
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            do {
                                let model = try decoder.decode(MODEL.self, from: data)
                                let responseModel = ResponseModel(model: model,
                                                                  responseCode: StatusCodes.init(rawValue: response.response!.statusCode)!)
                                promise(.success(responseModel))
                            } catch(let error) {
                                fatalError("Cannot decode model, error message: \(error)")
                            }
                        case .failure(let error):
                            promise(.failure(error))
                        }
                    case StatusCodes.code401Unauthorised.rawValue:
                        let responseModel = ResponseModel<MODEL>(model: nil,
                                                                 responseCode: StatusCodes.init(rawValue: response.response!.statusCode)!)
                        promise(.success(responseModel))
                    case StatusCodes.code403Forbidden.rawValue:
                        let responseModel = ResponseModel<MODEL>(model: nil,
                                                                 responseCode: StatusCodes.init(rawValue: response.response!.statusCode)!)
                        promise(.success(responseModel))
                    case StatusCodes.code404NotFound.rawValue:
                        let responseModel = ResponseModel<MODEL>(model: nil,
                                                                 responseCode: StatusCodes.init(rawValue: response.response!.statusCode)!)
                        promise(.success(responseModel))
                    default:
                        fatalError("Not supported status: \(String(describing: response.response?.statusCode))")
                    }
                }
        }.eraseToAnyPublisher()
    }
}
