//
//  ResponseModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/02/2022.
//

import Foundation

struct ResponseModel<MODEL: Codable> {
    let model: MODEL?
    let responseCode: StatusCodes
}
