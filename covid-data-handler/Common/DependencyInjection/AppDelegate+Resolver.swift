//
//  AppDelegate+Resolver.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 14/02/2022.
//

import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        registerNewReleasesRepository()
        registerFeaturedPlaylistsRepository()
    }
}
