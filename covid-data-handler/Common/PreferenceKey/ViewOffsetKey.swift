//
//  ViewOffsetKey.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 23/01/2022.
//

import SwiftUI

struct ViewOffsetKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat.zero
    
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value += nextValue()
    }
}
