//
//  TokenManager.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/02/2022.
//

import Foundation
import SwiftKeychainWrapper

class TokenUtil {
    static func saveAccessToken(authToken: String, expiresInSeconds: Int) {
        let tokenValidUntilTimestamp = Int(NSDate().timeIntervalSince1970) + expiresInSeconds
        KeychainWrapper.standard.set(authToken, forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_KEY)
        KeychainWrapper.standard.set(tokenValidUntilTimestamp, forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_VALID_UNITL_KEY)
        KeychainWrapper.standard.set(authToken, forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_KEY)
    }
    
    static func saveRefreshToken(refreshToken: String) {
        KeychainWrapper.standard.set(refreshToken, forKey: AuthManager.KEYCHAIN_STATICS.REFRESH_TOKEN_KEY)
    }
    
    static func isAccessTokenValid() -> Bool {
        guard KeychainWrapper.standard.string(forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_KEY) != nil,
              let accessTokenExpiredInTimestamp = KeychainWrapper.standard.integer(forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_VALID_UNITL_KEY)
        else { return false }
        
        let fiveMinutesInSeconds = 300
        let validTokenDate = accessTokenExpiredInTimestamp - fiveMinutesInSeconds
        
        let currentDate = Int(Date().timeIntervalSince1970)
        return currentDate <= validTokenDate
    }
    
    static func isRefreshTokenExist() -> Bool {
        return KeychainWrapper
            .standard
            .string(forKey: AuthManager.KEYCHAIN_STATICS.REFRESH_TOKEN_KEY) != nil
    }
    
    static func getAuthToken() throws -> String {
        guard let accessToken = KeychainWrapper.standard.string(forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_KEY) else {
            throw AuthError.accessTokenNotFound
        }
        return accessToken
    }
    
    static func deleteAllTokens() {
        KeychainWrapper
            .standard
            .removeObject(forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_KEY)
        KeychainWrapper
            .standard
            .removeObject(forKey: AuthManager.KEYCHAIN_STATICS.ACCESS_TOKEN_VALID_UNITL_KEY)
        KeychainWrapper
            .standard
            .removeObject(forKey: AuthManager.KEYCHAIN_STATICS.REFRESH_TOKEN_KEY)
    }
}
