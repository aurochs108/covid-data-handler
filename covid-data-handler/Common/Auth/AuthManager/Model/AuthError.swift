//
//  AuthError.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 09/02/2022.
//

import Foundation

enum AuthError: Error {
    case accessTokenNotFound
    case refreshNotPossible
    case refreshTokenNotExist
}
