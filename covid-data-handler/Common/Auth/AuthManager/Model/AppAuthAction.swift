//
//  AppAuthState.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/02/2022.
//

enum AppAuthAction {
    case showApp
    case showLoginPage
    case error
    case loading
    case refreshAccessToken
}
