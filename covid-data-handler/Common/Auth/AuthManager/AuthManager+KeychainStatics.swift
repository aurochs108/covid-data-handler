//
//  AuthManager+KeychainStatics.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 22/12/2021.
//

extension AuthManager {
    enum KEYCHAIN_STATICS {
        static let ACCESS_TOKEN_KEY = "Access_Token"
        static let ACCESS_TOKEN_VALID_UNITL_KEY = "access_token_valid_until"
        static let REFRESH_TOKEN_KEY = "refresh_token"
    }
}

