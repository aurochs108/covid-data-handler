//
//  ProjectCalloutBoldModifier.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 31/01/2022.
//

import SwiftUI

struct ProjectCalloutBoldModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(.white)
            .font(.callout.weight(.bold))
            .lineLimit(2)
    }
}
