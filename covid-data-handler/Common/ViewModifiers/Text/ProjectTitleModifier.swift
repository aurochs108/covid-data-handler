//
//  ProjectFonts.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import SwiftUI

struct ProjectTitleModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.title.weight(.bold))
            .foregroundColor(.white)
    }
}
