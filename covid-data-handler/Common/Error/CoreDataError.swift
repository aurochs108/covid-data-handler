//
//  CoreDataError.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 16/01/2022.
//

import Foundation

enum CoreDataError: Error {
    case noRecord
    case cannotFetchRecord
}
