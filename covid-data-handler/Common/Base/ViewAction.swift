//
//  ViewAction.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/12/2021.
//

import Foundation

enum ViewAction {
    case reload
}
