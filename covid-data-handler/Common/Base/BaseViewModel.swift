//
//  BaseViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/12/2021.
//

import Foundation
import Combine
import CoreData
import os

class BaseViewModel: ObservableObject {
    @Published var viewState: ViewState = .loading
    lazy var dataAction = PassthroughSubject<ViewAction, Never>()
    lazy var cancelBag = Set<AnyCancellable>()
    lazy var context: NSManagedObjectContext = PersistenceController.shared.container.viewContext
    var logger: Logger!
    
    init() {
        self.logger = Logger(subsystem: Bundle.main.bundleIdentifier!,
                             category: String(describing: self))
        logger.debug("Init")
    }
    
    deinit {
        logger.debug("Deinit")
    }
}
