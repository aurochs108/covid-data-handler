//
//  ParameterKeyStatics.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 23/01/2022.
//

import Foundation

enum ParameterKeyStatic: String {
    case viewOffsetKey = "offset"
}
