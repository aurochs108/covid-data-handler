//
//  PreviewStatics.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 09/01/2022.
//

import Foundation

enum PreviewStatics {
    static let randomImageURL = URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Anelsonia_eurycarpa_95429765.jpg/1920px-Anelsonia_eurycarpa_95429765.jpg")!
    static let randomTitle = "Random title"
    static let randomActionText = "Random action!"
}
