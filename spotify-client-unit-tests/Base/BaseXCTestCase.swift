//
//  BaseXCTestCase.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 13/03/2022.
//

@testable import covid_data_handler
import Foundation
import XCTest
import Resolver
import Combine

class BaseXCTestCase: XCTestCase {
    lazy var cancelBag = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        Resolver.registerMockServices()
    }
}
