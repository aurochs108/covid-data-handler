//
//  AlbumTracksUnitTests.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 10/03/2022.
//

@testable import covid_data_handler
import Resolver
import XCTest
import Combine

class AlbumTracksUnitTests: BaseXCTestCase {
    var sut: AlbumTracksViewModel!
    let playlistMockModel = PlaylistMockModel(description: String.random(length: 10),
                                              mainImage: PreviewStatics.randomImageURL,
                                              name: String.random(length: 8),
                                              artistsNames: String.random(length: 10),
                                              type: MusicType.allCases.randomElement()!)
    

    override func setUpWithError() throws {
        try super.setUpWithError()
        
        sut = .init(albumId: "randomId",
                    albumModel: playlistMockModel,
                    releaseYear: "2001")
        
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testFilterPlaylistItemsByProperArtistName() throws {
        let promise = expectation(description: "Filter playlist items by proper artist name and check results")
        
        // given
        let properArtistName = "Imagine Dragons"
        
        sut.$viewState.filter { $0 == .show }
            .sink { _ in
                // when
                self.sut.filterPlaylistItems(input: properArtistName)
                
                // then
                XCTAssertEqual(self.sut.filteredItemsModels.count, 1)
                XCTAssertEqual(self.sut.filteredItemsModels.first?.artistsNames, properArtistName)
                
                promise.fulfill()
        }.store(in: &cancelBag)
        
        wait(for: [promise], timeout: 5)
    }
    
    func testFilterPlaylistItemsByProperPartOfArtistName() throws {
        let promise = expectation(description: "Filter playlist items by proper part of artist name and check results")
        
        // given
        let properPartArtistName = "ragons"
        let properArtistName = "Imagine Dragons"
        
        sut.$viewState.filter { $0 == .show }
            .sink { _ in
                // when
                self.sut.filterPlaylistItems(input: properPartArtistName)
                
                // then
                XCTAssertEqual(self.sut.filteredItemsModels.count, 1)
                XCTAssertEqual(self.sut.filteredItemsModels.first?.artistsNames, properArtistName)
                
                promise.fulfill()
        }.store(in: &cancelBag)
        
        wait(for: [promise], timeout: 5)
    }
    
    func testFilterPlaylistItemsByWrongArtistName() throws {
        let promise = expectation(description: "Filter playlist items by wrong artist name and check results")
        
        // given
        let wrongArtistName = "Some bad artist name"
        
        sut.$viewState.filter { $0 == .show }
            .sink { _ in
                // when
                self.sut.filterPlaylistItems(input: wrongArtistName)
                
                // then
                XCTAssertEqual(self.sut.filteredItemsModels.count, 0)
                XCTAssertNotEqual(self.sut.filteredItemsModels.first?.artistsNames, wrongArtistName)
                
                promise.fulfill()
        }.store(in: &cancelBag)
        
        wait(for: [promise], timeout: 5)
    }
    
    func testFilterPlaylistItemsByProperSongName() throws {
        let promise = expectation(description: "Filter playlist items by proper song name and check results")
        
        // given
        let properSongName = "Bones"
        
        sut.$viewState.filter { $0 == .show }
            .sink { _ in
                // when
                self.sut.filterPlaylistItems(input: properSongName)
                
                // then
                XCTAssertEqual(self.sut.filteredItemsModels.count, 1)
                XCTAssertEqual(self.sut.filteredItemsModels.first?.name, properSongName)
                
                promise.fulfill()
        }.store(in: &cancelBag)
        
        wait(for: [promise], timeout: 5)
    }
    
    func testFilterPlaylistItemsByProperPartOfSongName() throws {
        let promise = expectation(description: "Filter playlist items by proper part of song name and check results")
        
        // given
        let properPartSongName = "ones"
        let properSongName = "Bones"
        
        sut.$viewState.filter { $0 == .show }
            .sink { _ in
                // when
                self.sut.filterPlaylistItems(input: properPartSongName)
                
                // then
                XCTAssertEqual(self.sut.filteredItemsModels.count, 1)
                XCTAssertEqual(self.sut.filteredItemsModels.first?.name, properSongName)
                
                promise.fulfill()
        }.store(in: &cancelBag)
        
        wait(for: [promise], timeout: 5)
    }
    
    
    func testFilterPlaylistItemsByWrongSongName() throws {
        let promise = expectation(description: "Filter playlist items by wrong song name and check results")
        
        // given
        let wrongSongName = "Some bad song name"
        
        sut.$viewState.filter { $0 == .show }
            .sink { _ in
                // when
                self.sut.filterPlaylistItems(input: wrongSongName)
                
                // then
                XCTAssertEqual(self.sut.filteredItemsModels.count, 0)
                XCTAssertNotEqual(self.sut.filteredItemsModels.first?.name, wrongSongName)
                
                promise.fulfill()
        }.store(in: &cancelBag)
        
        wait(for: [promise], timeout: 5)
    }
}
