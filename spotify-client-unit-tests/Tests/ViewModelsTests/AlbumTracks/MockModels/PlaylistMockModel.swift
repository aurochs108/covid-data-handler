//
//  PlaylistMockModel.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 12/03/2022.
//

@testable import covid_data_handler
import Foundation

struct PlaylistMockModel: PlaylistModelProtocol {
    let description: String?
    let mainImage: URL?
    let name: String
    let artistsNames: String
    let type: MusicType
}
